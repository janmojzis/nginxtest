import sys

try:
    branch = sys.argv[1]
except:
    branch = 'UNKNOWN'

if branch == 'main':
    packages = {
        'nginx': {'env': {'URL': 'https://salsa.debian.org/nginx-team/nginx', 'BRANCH':'experimental'} },
        'libnginx-mod-http-ndk': { 'deps': ['nginx',], },
        'libnginx-mod-http-auth-pam': { 'deps': ['nginx',] },
        'libnginx-mod-http-brotli': { 'deps': ['nginx',], 'package': 'libnginx-mod-http-brotli-filter libnginx-mod-http-brotli-static'},
        'libnginx-mod-http-cache-purge': { 'deps': ['nginx',] },
        'libnginx-mod-http-dav-ext': { 'deps': ['nginx',] },
        'libnginx-mod-http-echo': { 'deps': ['nginx',] },
        'libnginx-mod-http-fancyindex': { 'deps': ['nginx',] },
        'libnginx-mod-http-geoip2': { 'deps': ['nginx',] },
        'libnginx-mod-http-headers-more-filter': { 'deps': ['nginx',] },
        'lua-resty-core': { 'env': {'URL': 'https://salsa.debian.org/lua-team/lua-resty-core'} },
        'libnginx-mod-http-lua': { 'deps': ['lua-resty-core', 'libnginx-mod-http-ndk'] },
        'libnginx-mod-http-memc': { 'deps': ['nginx',] },
        'libnginx-mod-http-set-misc': { 'deps': ['libnginx-mod-http-ndk',] },
        'libnginx-mod-http-srcache-filter': { 'deps': ['nginx',] },
        'libnginx-mod-http-subs-filter': { 'deps': ['nginx',] },
        'libnginx-mod-http-uploadprogress': { 'deps': ['nginx',] },
        'libnginx-mod-http-upstream-fair': { 'deps': ['nginx',] },
        'libnginx-mod-js': { 'deps': ['nginx',], 'package': 'libnginx-mod-http-js libnginx-mod-stream-js' },
        'libnginx-mod-nchan': { 'deps': ['nginx',] },
        'libnginx-mod-rtmp': { 'deps': ['nginx',] },
    }
    # XXX - nginx-extras
    nginx_packages = ['nginx', 'nginx-core', 'nginx-full', 'nginx-light', 'nginx-common']
else:
    packages = {
        'nginx': {'env': {'URL': 'https://salsa.debian.org/nginx-team/nginx', 'BRANCH':'experimental'} },
        'libnginx-mod-http-brotli': { 'deps': ['nginx',], 'package': 'libnginx-mod-http-brotli-filter libnginx-mod-http-brotli-static'},
        #'lua-resty-lrucache': { 'env': {'URL': 'https://salsa.debian.org/lua-team/lua-resty-lrucache'} },
    }
    nginx_packages = ['nginx']

# include 
print('include: nginx.yml')
print('')

# build
for package in packages:
    print(f'build {package}:')
    print('  extends: .build')
    if 'deps' in packages[package]:
        deps=packages[package]['deps']
        print('  needs:')
        for dep in deps:
            print(f"    - job: 'build {dep}'")
            print(f'      artifacts: true')
    else:
        print('  dependencies: []')
    if 'env' in packages[package]:
        variables = packages[package]['env']
        print('  variables:')
        for variable in variables:
            value = variables[variable]
            print(f'    {variable}: {value}')
            
    print('')

# apt
print('install:')
print('  extends: .install')
print('  needs:')
ipackages=''
for package in packages:
    if 'package' in packages[package]:
        ipackage = packages[package]['package']
    else:
        ipackage = package
    ipackages = f'{ipackages} {ipackage}'
    print(f"    - job: 'build {package}'")
    print('      artifacts: true')
print('  variables:')
print(f'    PACKAGES: "{ipackages}"')
print('')

# upgrade
for package in nginx_packages:
    print(f'upgrade {package}:')
    print('  extends: .upgrade')
    print('')
