all: .gitlab-ci.yml

.gitlab-ci.yml: gen.py
	python3 gen.py `git symbolic-ref --short -q HEAD` > .gitlab-ci.yml
